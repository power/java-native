package cn.har01d.demo.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;

public final class NativeLoader {
    private static final String NATIVE_PATH_PREFIX = "java_native";

    private static File tempDir;

    private NativeLoader() {
        throw new AssertionError();
    }

    public static void loadLibFromJar(String path) {
        if (path == null || !path.startsWith("/")) {
            throw new IllegalArgumentException("The path has to start with '/'.");
        }

        String[] parts = path.split("/");
        if (parts.length < 2) {
            throw new IllegalArgumentException("Invalid path.");
        }
        String filename = parts[parts.length - 1];

        if (tempDir == null) {
            tempDir = createTempDirectory(NATIVE_PATH_PREFIX);
            tempDir.deleteOnExit();
        }

        File temp = new File(tempDir, filename);

        try (InputStream is = NativeLoader.class.getResourceAsStream(path)) {
            if (is == null) {
                throw new FileNotFoundException("Cannot find the file " + path + " in classpath.");
            }
            Files.copy(is, temp.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            temp.delete();
            throw new IllegalStateException(e);
        }

        try {
            System.load(temp.getAbsolutePath());
        } finally {
            if (isPosixCompliant()) {
                temp.delete();
            } else {
                temp.deleteOnExit();
            }
        }
    }

    private static boolean isPosixCompliant() {
        try {
            return FileSystems.getDefault()
                    .supportedFileAttributeViews()
                    .contains("posix");
        } catch (FileSystemNotFoundException | ProviderNotFoundException | SecurityException e) {
            return false;
        }
    }

    private static File createTempDirectory(String prefix) {
        String tempDir = System.getProperty("java.io.tmpdir");
        File generatedDir = new File(tempDir, prefix + System.nanoTime());

        if (!generatedDir.mkdir()) {
            throw new IllegalStateException("Cannot create temp directory " + generatedDir.getName());
        }

        return generatedDir;
    }
}
