export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
echo "---编译动态库---"
g++ -fPIC -I ${JAVA_HOME}/include/ -I ${JAVA_HOME}/include/linux/ -shared -o libHelloNative.so cn_har01d_demo_HelloNative.c
cp libHelloNative.so ../resources/

cd cn/har01d/demo || exit
echo "---编译Java类---"
javac HelloNative.java

cd ../../../
echo "---运行Java程序---"
java -cp . cn.har01d.demo.HelloNative
