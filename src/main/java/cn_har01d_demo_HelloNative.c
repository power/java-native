#include "cn_har01d_demo_HelloNative.h"
#include <stdio.h>

// g++ -fPIC -I ${JAVA_HOME}/include/ -I ${JAVA_HOME}/include/linux/ -shared -o libHelloNative.so cn_har01d_demo_HelloNative.c
JNIEXPORT void JNICALL Java_cn_har01d_demo_HelloNative_greeting(JNIEnv *env, jclass cl, jstring name) {
    const char *nativeString = env->GetStringUTFChars(name, NULL);

    printf("Hello %s!\n", nativeString);

    env->ReleaseStringUTFChars(name, nativeString);
}
