package cn.har01d.demo;

import cn.har01d.demo.util.NativeLoader;

public class HelloNative {
    static {
        NativeLoader.loadLibFromJar("/libHelloNative.so");
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            for (String arg : args) {
                HelloNative.greeting(arg);
            }
        } else {
            HelloNative.greeting("World");
        }
    }

    public static native void greeting(String name);
}
